function load_template(id, callback = ({ wrapper}) => {}) {
    let wrapper = document.querySelector('#wrapper');
    let template = document.getElementById(id);
    let clone = document.importNode(template.content, true);

    wrapper.innerHTML = '';
    wrapper.appendChild(clone);

    callback({ wrapper });
}

function start(name) {
    window.localStorage.setItem('player', name);

    window.location.href = 'game.html';
}

function inputChange(value) {
    let button = document.querySelector('button#start');

    if (value === '') {
        button.setAttribute('disabled', null)
    } else {
        button.removeAttribute('disabled')
    }
}