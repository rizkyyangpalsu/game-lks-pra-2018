class Gem {
    constructor(row, column, image) {
        this.row = row;
        this.column = column;
        this.image = "assets/" + image + ".png";
        this.select = false;
    }

    destroy() {
        game.maps[this.row][this.column].image = null;
    }

    check() {
        let items = game.maps;

        if (this.select) {
            if (items[this.row][this.column + 1] !== undefined && items[this.row][this.column + 1].select === true) {
                items[this.row][this.column].select = false;
                items[this.row][this.column + 1].select = false;

                let jewel = items[this.row][this.column].image;
                items[this.row][this.column].image = items[this.row][this.column + 1].image;
                items[this.row][this.column + 1].image = jewel;

                game.render()
            } else if(items[this.row][this.column - 1] !== undefined && items[this.row][this.column - 1].select === true) {
                items[this.row][this.column].select = false;
                items[this.row][this.column - 1].select = false;

                let jewel = items[this.row][this.column].image;
                items[this.row][this.column].image = items[this.row][this.column - 1].image;
                items[this.row][this.column - 1].image = jewel;

                game.render()
            } else if(items[this.row + 1] !== undefined && items[this.row + 1][this.column].select === true) {
                items[this.row][this.column].select = false;
                items[this.row + 1][this.column].select = false;

                let jewel = items[this.row][this.column].image;
                items[this.row][this.column].image = items[this.row + 1][this.column].image;
                items[this.row + 1][this.column].image = jewel;

                game.render()
            } else if(items[this.row - 1] !== undefined && items[this.row - 1][this.column].select === true) {
                items[this.row][this.column].select = false;
                items[this.row - 1][this.column].select = false;

                let jewel = items[this.row][this.column].image;
                items[this.row][this.column].image = items[this.row - 1][this.column].image;
                items[this.row - 1][this.column].image = jewel;

                game.render()
            }
        } else {
            return this;
        }
    }

    // checkRule() {
    //     let items = game.maps;
    //
    //     try {
    //         if (items[this.row][this.column].image === items[this.row][this.column - 1].image && items[this.row][this.column].image === items[this.row][this.column + 1].image) {
    //             items[this.row][this.column].destroy();
    //             items[this.row][this.column - 1].destroy();
    //             items[this.row][this.column + 1].destroy();
    //
    //             game.score += 9;
    //
    //             return false;
    //         } else if (items[this.row][this.column].image === items[this.row - 1][this.column].image && items[this.row][this.column].image === items[this.row + 1][this.column].image) {
    //             items[this.row][this.column].destroy();
    //             items[this.row - 1][this.column].destroy();
    //             items[this.row + 1][this.column].destroy();
    //
    //             game.score += 9;
    //
    //             return false;
    //         }
    //     } catch (e) {
    //         return false;
    //     }
    //
    //     return false;
    // }
    
    // checkRule() {
    //     let items = game.maps;
    //    
    //     if (items[this.row][this.column + 1].image !== undefined && items[this.row][this.column + 2].image !== undefined) {
    //         if (items[this.row][this.column].image === items[this.row][this.column + 1].image && items[this.row][this.column + 1].image === items[this.row][this.column + 2].image) {
    //             if (items[this.row][this.column + 2].image === items[this.row][this.column + 3].image) {
    //                 if (items[this.row][this.column + 3].image === items[this.row][this.column + 4].image) {
    //                     items[this.row][this.column].destroy();
    //                     items[this.row][this.column + 2].destroy();
    //                     items[this.row][this.column + 1].destroy();
    //                     items[this.row][this.column + 3].destroy();
    //                     items[this.row][this.column + 4].destroy();
    //
    //                     game.checkJewelDestroyed();
    //                     return false;
    //                 }
    //                 items[this.row][this.column].destroy();
    //                 items[this.row][this.column + 1].destroy();
    //                 items[this.row][this.column + 2].destroy();
    //                 items[this.row][this.column + 3].destroy();
    //
    //                 game.checkJewelDestroyed();
    //                 return false;
    //             }
    //             items[this.row][this.column].destroy();
    //             items[this.row][this.column + 1].destroy();
    //             items[this.row][this.column + 2].destroy();
    //
    //             game.checkJewelDestroyed();
    //             return false;
    //         }
    //     } else if (items[this.row + 1][this.column].image !== undefined && items[this.row + 2][this.column].image !== undefined ) {
    //         if (items[this.row][this.column].image === items[this.row + 1][this.column].image && items[this.row + 1][this.column].image === items[this.row + 2][this.column].image) {
    //             items[this.row][this.column].destroy();
    //             items[this.row + 1][this.column].destroy();
    //
    //             if (items[this.row + 2][this.column].image === items[this.row + 3][this.column].image) {
    //                 items[this.row + 2][this.column].destroy();
    //
    //                 if (items[this.row + 3][this.column].image === items[this.row + 4][this.column].image) {
    //                     items[this.row + 3][this.column].destroy();
    //                     items[this.row + 4][this.column].destroy();
    //
    //                     game.checkJewelDestroyed();
    //                     return false;
    //                 }
    //                 items[this.row + 3][this.column].destroy();
    //
    //                 game.checkJewelDestroyed();
    //                 return false;
    //             }
    //             items[this.row + 2][this.column].destroy();
    //
    //             game.checkJewelDestroyed();
    //             return false;
    //         }
    //     } 
    // }
    
    checkRule() {
        let items = game.maps;
        
        for (let count = 0; count <= 4; count++) {
            if (items[this.row][this.column + count] !== undefined) {

            }
        }
    }

    getGem() {
        let td = document.createElement('td');
        let items = game.maps;

        td.setAttribute('id', 'tile_' + this.row + '_' + this.column);

        let img = document.createElement('img');
        img.setAttribute('src', this.image);

        td.appendChild(img);

        td.onclick = () => {
            if (this.select) {
                this.select = false;
                game.render()
            } else {
                this.select = true;
                this.check();
                game.render()
            }
        };

        if (this.select) {
            td.setAttribute('class', 'hold animate-slide');

            if (document.querySelectorAll('.hold').length > 1) {
                game.maps.forEach((value) => {
                    value.forEach((item) => {
                        item.select = false;
                    })
                });
            }

            document.querySelector('.container').onclick = () => {
                game.maps.forEach((value) => {
                    value.forEach((item) => {
                        item.select = false;
                    })
                });

                game.render();
            };
        } else {
            document.querySelector('.container').onclick = () => {
            };
        }

        return td;
    }
}